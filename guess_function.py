from itertools import combinations as nCr

ALPHABETS = "abcdefghijklnopqrstuvwxyz"
file = open("5letters.txt")

def check_word(feedback: str) -> str:
    if 5 in feedback and "win" in feedback:
        return "Game Over"
    else:
        new_word("""number from feedback, word for which the feedback was given""")

def new_word(n: int, prev_word: str) -> str:
    pass