from itertools import combinations as nCr
import random

ALPHABETS = "abcdefghijklnopqrstuvwxyz"

def load_file():
    file = open("5letters.txt", 'r')
    words = file.read().splitlines()
    print(words)

def check_word(feedback: int, message: str) -> str:
    if 5 in feedback and "win" in message:
        return "Game Over"
    else:
        new_word(filter_words())

def filter_words(n: int, prev_word: str) -> list:
    if n == 0:
        for i in prev_word:
            ALPHABETS = ALPHABETS.replace(i, '')
        letters = list(ALPHABETS)

    elif n == 1:
        letters = [i for i in prev_word and i in ALPHABETS]

    else:
        letters = list(nCr(prev_word, n))

def new_word(l: list) -> str:
    possible_words = []
    return random.choice(possible_words)
