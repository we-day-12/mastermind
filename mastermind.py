import requests as rq
import json
mm_url = "https://we6.talentsprint.com/wordle/game/"  
register_url = mm_url + "register"
register_dict = {"mode" :"Mastermind" , "name" :"anika"}
register_with = json.dumps(register_dict)


r = rq.post(register_url , json = register_dict)
print(r.text)
me = r.json()['id']
create_url = mm_url + "create"
create_dict = {"id" : me , "overwrite" :True}
rc= rq.post(create_url,json=create_dict)

session = rq.Session()
resp = session.post(register_url , json = register_dict)
resp.json()
me  =resp.json()['id']
print(me)
creat_dict = {"id" :me , "overwrite" :True}
rc = session.post (create_url,json = creat_dict)
print(rc.json())


guess = {"id" : me , "guess" : "proud"}
#guess = {"id" :  , "guess" : "path_to_guessFunction"}

guess_url = mm_url + "guess"
correct = session.post(guess_url , json=guess)

print(correct.json())


import random


def load_file(file: str) -> list:
    file = open(file, 'r')
    return [file.read().splitlines()]


def check_word(feedback: int, message: str) -> str:
    if 5 in feedback and "win" in message:
        return "Game Over"
    else:
        new_word()

def new_word(n: int, prev_word: str) -> str:
    words = load_file()
    for word in words:
        if len(set(word) & set(prev_word)) != n:
            words.remove(word)
    return random.choice(words)



